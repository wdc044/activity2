package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//Rest controller allows us to setup the main class as a rest controllers
@RestController
public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}

	// Get mapping indicates the url i.e. like web.php
	@GetMapping("/hi")
	//Request param adds GET PARAM to URL
	public String hello(@RequestParam(name = "user", defaultValue = "boss") String value){
		return String.format("Wassup %s", value);
	}
}
