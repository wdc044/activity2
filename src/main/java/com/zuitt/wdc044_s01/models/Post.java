package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

//mark this Java object as database table
@Entity
//name the table
@Table(name="posts")
public class Post {
    //indicate the primary key
    @Id
    //autoincrement
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    //default constructor neede when retrivving posts
    public  Post(){}

    //other necessary constructors
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }
}
