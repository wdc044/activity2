package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    //indicate the primary key
    @Id
    //autoincrement
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    public  User(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}
